package rechnerAufgabe;

import java.util.Scanner;

public class Rechner {
    public static void main(String[] args) {
        while (true) {
            String eingabe1;
            int zahl1, zahl2;
       //     int zahl3; internet sag int bei manchen websites oder long bei anderen... mal gucken was klappt
            long zahl3;
            

            System.out.println("Geben Sie eine ganze Zahl ein (x zum beenden): ");
            eingabe1 = new Scanner(System.in).nextLine();

            if (eingabe1.equalsIgnoreCase("x"))
                break;

            try {
                zahl1 = Integer.parseInt(eingabe1);
                System.out.println("Geben Sie eine ganze Zahl ein: ");
                zahl2 = Integer.parseInt(new Scanner(System.in).nextLine());

                //int zahl3 = zahl1 + zahl2; warum will das nicht funktionieren?
                zahl3 = zahl1 + zahl2;  //meep 
                if(zahl3 > Integer.MAX_VALUE ) {
                	
                	throw new ExceptionForOverflow("Overflow bei Addition");
                }
                
              //int zahl3 = zahl1 - zahl2; 
                zahl3 = zahl1 - zahl2; 
                
                if(zahl3 > Integer.MAX_VALUE ) {
                	throw new ExceptionForOverflow("Overflow bei Subtraktion");
                }
                
                //int zahl3 = zahl1 * zahl2; 
                zahl3 = (long) zahl1 * zahl2;  //(long) als ersatz f�r int --> ohne (long) wird die exception nicht beachtet
                
                if(zahl3 > Integer.MAX_VALUE || zahl3 < Integer.MIN_VALUE ) {
                	throw new ExceptionForOverflow("Overflow bei Multiplikation");
                }
              //int zahl3 = zahl1 / zahl2; 
                zahl3 = (long) zahl1 / zahl2;  //meep
                
                if(zahl3 < Integer.MIN_VALUE ) {
                	throw new ExceptionForOverflow("Underflow bei Division");
                }
                
              
                
                System.out.printf("%d + %d = %d%n", zahl1, zahl2, zahl1 + zahl2);
                System.out.printf("%d - %d = %d%n", zahl1, zahl2, zahl1 - zahl2);
                System.out.printf("%d * %d = %d%n", zahl1, zahl2, zahl1 * zahl2);
                System.out.printf("%d / %d = %d%n", zahl1, zahl2, zahl1 / zahl2);

            } 	catch (ArithmeticException ae) {
                System.err.println("\n Keine Division durch 0! Das lernt man doch in der Grundschule! \n");

            } 	catch (NumberFormatException nfe) {
                System.err.print("\n Das geht leider nicht. Gib bitte eine ganze Zahl ein. \n");

            }
            	catch (ExceptionForOverflow ee) {
                System.out.println(ee.toString());
                System.out.println(); //" \n OVERFLOOOOOOOOOOOOOOOOOOOOOOW \n "
            }

        }
    }
}