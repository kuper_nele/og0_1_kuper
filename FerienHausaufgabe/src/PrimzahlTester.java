import java.util.Scanner;


public class PrimzahlTester {

	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		Primzahl primzahl = new Primzahl(); 
		
		

		long x = scan.nextLong();
		long timestart = System.currentTimeMillis();
		System.out.println(primzahl.isPrimzahl(x));
		long timeend = System.currentTimeMillis();
		long time = timeend - timestart;
		System.out.println("Dauer: " + time + " ms ");
		scan.close();
	}
}

