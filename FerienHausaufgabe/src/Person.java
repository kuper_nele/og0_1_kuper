
	public class Person implements Comparable<Person>{

	    private String name;
	    private int birthdate;

	    public Person(String name, int birthdate){
	        this.name = name;
	        this.birthdate = birthdate;
	    }

	    public int getBirthdate() {
	        return birthdate;
	    }

	    @Override
	    public int compareTo(Person o) {

	        Person other = (Person) o;

	        if(this.getBirthdate() > other.getBirthdate()){
	            return 1;
	        }
	        else if(this.getBirthdate() < other.getBirthdate()){
	            return -1;
	        }
	        else{
	            return 0;
	        }
	        
	        
	    }
	
	
	@Override
	public String toString() {
	    return "Person{" + "name='" + name + '\'' + ", birthdate=" + birthdate + '}';
	}

	}