package LineareSuche;

public class LineareSuche {
	
	final int NICHT_GEFUNDEN = -1;
	
	public int suche (long[] zahlen, long gesuchteZahl) {
		for (int i = 0; i < zahlen.length; i++)
			if (zahlen[i] == gesuchteZahl)
				return i;
		return NICHT_GEFUNDEN;
		
	}

}
