package LineareSuche;

public class BinaereSuche {
	
	public int binaereSuche(int[] a, int x) {
		int mitte;
		int links,rechts;
		links = 0;
		rechts = a.length - 1;
		
		while (links <= rechts) {
			mitte = (links + rechts ) / 2;
			if(a[mitte] == x)
				return mitte;
			if (x<a[mitte]) rechts = mitte - 1;
			else links = mitte + 1;
		}
		return -1;
	}
	
	public int getVersuche(int[] a, int x) {
		int vergleiche = 0;
		
		for (int i = 0; i<zahlen.length; i++) {
			vergleiche ++;
				if (zahlen[i] == gesuchteZahl)
					return vergleiche;
			}
			return vergleiche;	
	}
	
	
	
}
