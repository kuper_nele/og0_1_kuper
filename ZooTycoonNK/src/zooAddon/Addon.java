package zooAddon;

public class Addon {

	private String name;
	private int idNr;
	private double preis;
	private int bestandSpieler;
	private int maxBestand;
	
	public Addon(String name, int idNr, double preis, int bestandSpieler, int maxBestand) {
		this.name = name;
		this.idNr = idNr;
		this.preis = preis;
		this.bestandSpieler = bestandSpieler;
		this.maxBestand = maxBestand;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIdNr() {
		return idNr;
	}

	public void setIdNr(int idNr) {
		this.idNr = idNr;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getBestandSpieler() {
		return bestandSpieler;
	}

	public void setBestandSpieler(int bestandSpieler) {
		this.bestandSpieler = bestandSpieler;
	}

	public int getMaxBestand() {
		return maxBestand;
	}

	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}
	public void kaufen () {
		bestandSpieler +=1;
			
	}
	public void verbrauchen () {
		bestandSpieler -=1;
	}
	
	public double getGesamtwertKauf() {
		return bestandSpieler * preis;
		
	}
}
