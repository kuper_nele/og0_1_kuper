package sortieralgorithmen;

import java.util.Arrays;

public class QuickSort {
	
	public int vertauschungen = 0;
	public int vergleiche = 0;
	

	public static int[] intArr = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };

    public int[] sort(int l, int r) {
        int q;
        if (l < r) {
            q = partition(l, r); 
            sort(l, q);
            sort(q + 1, r);
            
            vergleiche++;
        }
        return intArr;
      
    }

    int partition(int l, int r) {

        int i, j, x = intArr[(l + r) / 2];
        i = l - 1;
        j = r + 1;
        while (true) {
            do {
                i++;
            } while (intArr[i] < x);

            do {
                j--;
            } while (intArr[j] > x);

            if (i < j) {
                int k = intArr[i];
                intArr[i] = intArr[j];
                intArr[j] = k;
                
                vertauschungen++;
            } else { 
            	 return j; 
            }
            }
               
            
       
   
        }
        
   

    public static void main(String[] args) {
        QuickSort qs = new QuickSort();
        
        System.out.println(Arrays.toString(intArr));
        
        int[] arr = qs.sort(0, intArr.length - 1);
        
        for (int i = 0; i < arr.length; i++) {
        	 System.out.println(i + 1 + ": " + arr[i]);
        
        
       
        
        }
        System.out.println(Arrays.toString(intArr));
        System.out.println("Vergleiche: "+qs.vergleiche);
        System.out.println("Vertauschungen: " +qs.vertauschungen);
        
    } 
	
}
